'use strict';

var path = require('path');

module.exports = function (app) {


  app.use('/api/public', require('./api/public/public.router'));
  app.use('/api', require('./api/security/security.router'));
  app.use('/api/users', require('./api/user/user.router'));
  app.use('/api/events', require('./api/event/event.router'));


  app.route('/*')
    .get(function (req, res) {
      res.sendFile(path.resolve(app.get('appPath') + '/index.html'));
    });
};
