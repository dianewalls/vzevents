'use strict';
angular.module('tellme')
  .config(function ($stateProvider) {
    $stateProvider
      .state('animals', {
        url: '/animals',
        templateUrl: 'app/animals/animals.view.html',
        controller: 'AnimalsCtrl'
      })
      .state('modal', {
        url: '/animals',
        templateUrl: 'app/animals/modal.html',
        controller: 'ModalCtrl'
      });

  });