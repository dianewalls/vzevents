'use strict';

var express = require('express');
var userController = require('../user/user.controller');
var eventController = require('../event/event.controller');
var router = express.Router();

router.post('/auth/sign_in', userController.authenticate);
router.post('/auth', userController.create);
router.delete('/auth/sign_out', userController.signout);

router.get('/events', eventController.list);
module.exports = router;
