'use strict';
angular.module('tellme').controller('ModalCtrl',

  function ($scope, $uibModalInstance, data) {
    console.log(data);

    $scope.exit = function () {
      $uibModalInstance.dismiss();
    };

    $scope.animal = data;

    $scope.edit = function (animal) {
      $uibModalInstance.close(animal);
    };
  }
);