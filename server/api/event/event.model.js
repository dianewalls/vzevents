'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var EventSchema = new Schema({
  image: String,
  title: String,
  subtitle: String,
  principal:  {type: Boolean, default: false},
});


module.exports = mongoose.model('Event', EventSchema);
