'use strict';

var express = require('express'),
  router = express.Router(),
  mongoose = require('mongoose'),
  _ = require('lodash'),
  util = require('../util');

var Schema = mongoose.Schema;
var UserSchema = new Schema({
  name: String,
  email: {type: String, lowercase: true},
  posts: Object
});
var User = mongoose.model('User', UserSchema);

router.get('/', function (req, res) {
  User.find({}, function (err, users) {
    if (err) return util.handleError(res, err);
    res.json(users);
  });
});

router.post('/', function (req, res) {
  var user = new User(req.body);
  user.save(function (err, user) {
    if (err) return util.handleError(res, err);
    util.handleOk(res, user);
  });
});

router.put('/:userId', function (req, res) {
  var userId = req.params.userId;
  User.findOne({_id: mongoose.Types.ObjectId(userId)}, function (err, user) {
    if (err) return util.handleError(res, err);
    if (!user) return util.notFound(res, "no user");
    user = _.merge(user, req.body);
    user.save(function (err) {
      if (err) return util.handleError(res, err);
      util.handleOk(res);
    });
  });
});

router.delete('/:userId', function (req, res) {
  var userId = req.params.userId;
  User.findOne({_id: mongoose.Types.ObjectId(userId)}, function (err, user) {
    if (err) return util.handleError(res, err);
    if (!user) return util.notFound(res, "no user");
    user.remove(function (err) {
      if (err) return util.handleError(res, err);
      util.handleOk(res);
    });
  });
});

module.exports = router; 