'use strict';
angular.module('tellme')
    .controller('SignupCtrl',  function($scope, $auth,  $rootScope, $location) {
        $scope.handleRegBtnClick = function() {
            $auth.submitRegistration($scope.registrationForm)
                .then(function(resp) {

                    console.log('Exito: '+resp);
                })
                .catch(function(resp) {
                    console.log('Exito: '+resp);
                });
        };

      $rootScope.$on('auth:registration-email-success', function(ev, resp) {

        if(resp.toString() === '1'){
          $auth.submitLogin($scope.registrationForm)
            .then(function(resp) {
              console.log(resp);
            })
            .catch(function(resp) {
              console.log(resp);
            });
        }
        if(resp.toString() === '2'){
          alert('Username or email already registered');
        }

      });

    });
