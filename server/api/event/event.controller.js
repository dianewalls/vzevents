'use strict';
var Event = require('./event.model');
var util = require('../../util');
var mongoose = require('mongoose');

exports.list = function (req, res) {
  Event.find({}, function (err, events) {
    if (err) return util.handleError(res, err);
    res.json(events);
  });
};

exports.create = function (req, res) {
  var event = new Event(req.body);
  event.save(function (err, event) {
    if (err) return util.handleError(res, err);
    util.handleOk(res, event);
  });
};


exports.update = function (req, res) {
  var eventId = req.params.eventId;
  Event.findOne({_id: mongoose.Types.ObjectId(eventId)}, function (err, event) {
    if (err) return util.handleError(res, err);
    if (!event) return util.notFound(res, "no event");
    event = _.merge(event, req.body);
    event.save(function (err) {
      if (err) return util.handleError(res, err);
      util.handleOk(res);
    });
  });
};


exports.delete = function (req, res) {
  var eventId = req.params.eventId;
  Event.findOne({_id: mongoose.Types.ObjectId(eventId)}, function (err, event) {
    if (err) return util.handleError(res, err);
    if (!event) return util.notFound(res, "no event");
    event.remove(function (err) {
      if (err) return util.handleError(res, err);
      util.handleOk(res);
    });
  });
};

