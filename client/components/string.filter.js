'use strict';

angular.module('tellme')
  .filter('stringShortener', function () {
    return function (input, length) {
      if (input) return input.substring(0, length);
      return '';
    };
  });