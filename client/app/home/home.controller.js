'use strict';

angular.module('tellme').controller('HomeCtrl', function ($scope, EventsService) {
  $scope.myInterval = 3000;
  $scope.noWrapSlides = false;
  var slides = $scope.slides = [];
  $scope.addSlide = function (event) {
    slides.push({
      image: event.image,
      title: event.title,
      subtitle: event.subtitle
    });
  };

  EventsService.list().then(function (events) {

    for (var i = 0; i < events.length; i++) {
      $scope.addSlide(events[i]);
    }
  }, function (err) {
    console.error(err);

  });




});
