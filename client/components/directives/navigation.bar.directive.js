/**
 * Created by dianewalls on 21/11/2015.
 */
'use strict';

angular.module('tellme')
  .directive('navigationBar', function () {
    return {
      restrict: 'E',
      templateUrl: '../../app/layout/navigation.bar.view.html'
    }
  });

