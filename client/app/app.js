'use strict';

angular.module('tellme', [
    'ngCookies',
    'ui.router',
    'ui.bootstrap',
    'ngAnimate',
    'ng-token-auth'
  ])
  .config(function ($urlRouterProvider, $authProvider, $locationProvider) {
    $urlRouterProvider
      .otherwise('/');
      $locationProvider.html5Mode(true);
      $authProvider.configure({
          apiUrl: '/api',
        emailRegistrationPath: '/public/auth',
        emailSignInPath: '/public/auth/sign_in',
        signOutUrl: '/public/auth/sign_out'
      });
  });
