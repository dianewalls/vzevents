'use strict';

var express = require('express'),
    app = express(),
    server = require('http').createServer(app),
    config = require('./config'),
    port = config.port,
    morgan = require('morgan'),
    mongoose = require('mongoose');
    mongoose.connect(config.mongo.uri, config.mongo.options);
var finalhandler = require('finalhandler');
var http = require('http');
var logger = morgan('combined');
http.createServer(function (req, res) {
    var done = finalhandler(req, res)
    logger(req, res, function (err) {
        if (err) return done(err)

        // respond to request
        res.setHeader('content-type', 'text/plain')
        res.end('hello, world!')
    })
})

mongoose.connection.on('error', function (err) {
    console.error("mongodb connection failed: ", err);
    process.exit(-1);
});

app.use(morgan('combined'));


require('./express')(app);

require('./routes')(app);

server.listen(port, function () {
    console.log('server started in port ', port);
});
