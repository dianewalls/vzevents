'use strict';

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var bcrypt = require('bcrypt-nodejs');
var SALT_WORK_FACTOR = 10;

// User schema
var UserSchema = new Schema({
  username: {type: String, required: true, unique: true},
  email: {type: String, required: true, unique: true, lowercase: true},
  password: {type: String, required: true},
  admin: {type: Boolean, default: false},
  created: {type: Date, default: Date.now}
});

// Bcrypt middleware on UserSchema
UserSchema.pre('save', function (next) {
  var user = this;

  console.log(user);
  if (!user.isModified('password')) return next();

  bcrypt.genSalt(SALT_WORK_FACTOR, function (err, salt) {
    if (err) return next(err);

    bcrypt.hash(user.password, salt, null, function (err, hash) {
      if (err) return next(err);
      user.password = hash;
      next();
    });
  });
});


//Password verification
UserSchema.methods.comparePassword = function (password, cb) {
  bcrypt.compare(password, this.password, function (err, isMatch) {
    if (err) return cb(err);
    cb(isMatch);
  });
};

module.exports = mongoose.model('User', UserSchema);