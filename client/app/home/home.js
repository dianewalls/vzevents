'use strict';

angular.module('tellme')
  .config(function ($stateProvider) {
    $stateProvider
      .state('home', {
        url: '/',
        templateUrl: 'app/home/home.view.html',
        controller: 'HomeCtrl'
      });
  });