'use strict';

angular.module('tellme').controller('AnimalsCtrl',
  function ($scope, AnimalsService, $uibModal) {

    $scope.myInterval = 3000;
    $scope.noWrapSlides = false;
    $scope.animationsEnabled = true;
    var slides = $scope.slides = [];

    $scope.abrir = function (slide) {

      $uibModal.open({
        animation: $scope.animationsEnabled,
        templateUrl: 'app/animals/modal.html',
        controller: 'ModalCtrl',

        resolve: {
          data: function () {
            return slide;
          }
        }
      });

    };

    $scope.addAnimal = function (animal) {
      slides.push({
        image: animal.image,
        name: animal.name

      });
    };

    AnimalsService.list().then(function (animales) {
      for (var i = 0; i < animales.length; i++) {
        $scope.addAnimal(animales[i]);

      }
    }, function (err) {
      console.error(err);

    });


  });
