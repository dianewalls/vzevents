'use strict';

exports.handleError = function (res, err) {
  res.status(500).send(err);
};

exports.notFound = function (res, msg) {
  res.status(404).send(msg);
};

exports.notAuthorized = function (res, msg) {
  res.status(401).send(msg);
};

exports.handleOk = function (res, msg) {
  if (msg) res.status(201).send(msg);
  res.status(201).send();
};



