'use strict';
angular.module('tellme')
  .controller('SettingsCtrl',
      function($scope, $auth) {
        $scope.handleUpdatePasswordBtnClick = function() {
          $auth.updatePassword($scope.updatePasswordForm)
              .then(function(resp) {
                // handle success response
              })
              .catch(function(resp) {
                // handle error response
              });
        };


        $scope.handleUpdateAccountBtnClick = function() {
          $auth.updateAccount($scope.updateAccountForm)
              .then(function(resp) {
                // handle success response
              })
              .catch(function(resp) {
                // handle error response
              });
        };

        $scope.handleDestroyAccountBtnClick = function() {
          $auth.destroyAccount()
              .then(function(resp) {
                // handle success response
              })
              .catch(function(resp) {
                // handle error response
              });
        };
  });


