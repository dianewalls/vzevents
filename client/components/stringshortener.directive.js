'use strict';

angular.module('tellme')
    .directive('backImg', function () {
    return function (scope, element, attrs) {
      var url = attrs.backImg;
      element.css({
        'background-image': 'url(' + url + ')',
        'background-color': '#888',
        'background-size': 'cover',
        'background-position': 'top center',
        'background-repeat': 'no-repeat',
        'height': 500
      });
    };
  })
  .directive('stringShortener', function () {
    return {
      restrict: 'E',
      scope: {
        string: '='
      },
      template: '<span>{{string}}</span>',
      link: function (scope, elem, attr) {
        console.log(attr);
        console.log(elem.attr);
        attr.class = 'string-shortener';
        scope.string = scope.string.substring(0, 2);
        elem.on('mouseenter', function (event) {
          event.preventDefault();
          // alert('mouse enter');


        });

        elem.css({
          color: 'red', "font-size": '30px'
        });

        console.log(attr);

      }
    }
  });

//binding = doble binding ,& no hace doble binding ,@ si hace pero con {{}}
