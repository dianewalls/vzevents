'use strict';

var express = require('express');
var securityController = require('./security.controller');

var router = express.Router();


router.use('/', securityController.verify);

module.exports = router;
