'use strict';

var config = {
  port: 1337,
  root: __dirname + '/..',
  mongo: {
    uri: "mongodb://127.0.0.1/vzevents",
    options: {
      db: {
        safe: true
      }
    }
  },
  secret: 'mariana'
};

module.exports = config;