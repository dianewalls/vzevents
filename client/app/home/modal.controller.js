'use strict';

angular.module('tellme').controller('mmmModalCtrl',

  function ($scope, $modalInstance, data) {
    $scope.exit = function () {
      $modalInstance.dismiss();
    };

    $scope.user = data;

    $scope.edit = function (user) {
      $modalInstance.close(user);
    };
  }
);