'use strict';

angular.module('tellme')
  .service('EventsService', function ($http, $q) {
    this.list = function () {
      var deferred = $q.defer();
      $http.get('api/public/events').then(function (res) {
        var events = res.data;
        deferred.resolve(events);
      }, function () {
        deferred.reject({msg: 'Error while retrieving events'});
      });
      return deferred.promise;
    };
  });
