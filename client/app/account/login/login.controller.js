'use strict';

angular.module('tellme')
  .controller('LoginCtrl', function ($scope, $auth, $location, $cookies, $rootScope) {
    $scope.handleLoginBtnClick = function () {
      $auth.submitLogin($scope.loginForm)
        .then(function (resp) {
          console.log(resp);
        })
        .catch(function (resp) {
          console.log(resp);
        });
    };

    $rootScope.$on('auth:login-success', function (ev, resp) {
      $cookies.putObject('user', resp);
      console.info(resp);
      $location.path('/events');
    });

    $rootScope.$on('auth:login-error', function (ev, reason) {
      console.info(reason);
      alert('auth failed because');
    });
  });
