

var config = require('./server/config');

module.exports = function (grunt) {

  // Load Grunt tasks declared in the package.json file
  grunt.loadNpmTasks('grunt-express-server');
  grunt.loadNpmTasks('grunt-injector');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-wiredep');


  grunt.initConfig({

    watch: {
      options: {
        livereload: true
      },
      express: {
        files: ['client/**/*.*'],
        tasks: ['express:dev'],
        options: {
          spawn: false,
          events: ['changed']
        }
      },
      css: {
        files: ['client/{app,components}/**/*.css'],
        tasks: ['injector:local_dependencies'],
        options: {
          events: ['deleted', 'added'],
          spawn: false
        }
      },
      scripts: {
        files: ['client/{app,components}/**/*.js'],
        tasks: ['injector:scripts'],
        options: {
          events: ['deleted', 'added'],
          spawn: false
        }
      }
    },

    express: {
      options: {
        port: config.port,
      },
      dev: {
        options: {
          script: 'server/app.js'
        }
      }
    },

    injector: {
      options: {},
      scripts: {
        options: {
          transform: function (filePath) {
            filePath = filePath.replace('/client/', '');
            return '<script src="' + filePath + '"></script>';
          },
          starttag: '<!-- injector:js -->',
          endtag: '<!-- endinjector -->'
        },
        files: {
          'client/index.html': [
            'client/{app,components}/**/*.js',
            //'client/**/*.js',
            '!client/app/app.js'
          ]
        }
      },
      local_dependencies: {
        options: {
          transform: function (filePath) {
            filePath = filePath.replace('/client/', '');
            return '<link href="' + filePath + '" rel="stylesheet" type="text/css" />';
          },
          starttag: '<!-- injector:css -->',
          endtag: '<!-- endinjector -->'
        },
        files: {
          'client/index.html': ['client/{app,components}/**/*.css'],
        }
      }
    },
    wiredep: {

      task: {

        // Point to the files that should be updated when
        // you run `grunt wiredep`
        src: [
          'client/index.html'
        ],

        options: {
          // See wiredep's configuration documentation for the options
          // you may pass:

          // https://github.com/taptapship/wiredep#configuration
        }
      }
    }
  });

  grunt.registerTask('wait', function () {
    grunt.log.ok('Waiting for server reload...');
    var done = this.async();
    setTimeout(function () {
      grunt.log.writeln('Done waiting!');
      done();
    }, 1500);
  });

  grunt.registerTask('express-keepalive', 'Keep grunt running', function () {
    this.async();
  });

  grunt.registerTask('serve', [
    'wiredep',
    'injector:scripts',
    'injector:local_dependencies',
    'express:dev',
    'watch'
  ]);

};
