'use strict';

angular.module('tellme')
  .config(function ($stateProvider) {
    $stateProvider
      .state('event', {
        url: '/events',
        templateUrl: 'app/event/events.view.html',
        controller: 'EventCtrl',
        authenticate: true
      })
  });
