'use strict';

angular.module('tellme')
  .config(function ($stateProvider) {
    $stateProvider
      .state('navigation', {
        templateUrl: 'app/layout/navigation.bar.view.html',
        controller: 'NavigationCtrl'
      });
  });
