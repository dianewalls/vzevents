'use strict';

var express = require('express');
var controller = require('./event.controller');
var config = require('../../config');

var router = express.Router();


router.post('/', controller.create);
router.put('/:eventId', controller.update);
router.delete('/:eventId', controller.delete);

module.exports = router;
