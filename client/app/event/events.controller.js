'use strict';
angular.module('tellme')
    .controller('EventCtrl', function($scope, $auth,$cookies,$rootScope, $location) {
      var user = $cookies.getObject('user');
      $rootScope.user=user;

      if(!user){
        $location.path('/');
      }
        $scope.handleSignOutBtnClick = function() {
            $auth.signOut()
                .then(function(resp) {

                })
                .catch(function(resp) {
                    // handle error response
                });
        };

      $rootScope.$on('auth:logout-success', function(ev) {
        $cookies.remove('user');
        $rootScope.user=null;
        $location.path('/');
      });
    });
