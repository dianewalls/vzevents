'use strict';
var User = require('./user.model');
var _ = require('lodash');
var jwt = require('jsonwebtoken');
var util = require('../../util');
var config = require('../../config');

exports.list = function (req, res) {
  User.find({}, function (err, users) {
    if (err) return util.handleError(res, err);
    res.json(users);
  });
};

exports.create = function (req, res) {
  var user = new User(req.body);
  User.findOne({$or: [ { username: user.username }, { email: user.email } ] }, function (err, resUser) {
    if (err) return util.handleError(res, err);
    if(resUser){
      util.handleOk(res, '2');
    }else{
      user.save(function (err) {
        if (err) return util.handleError(res, err);
        util.handleOk(res, '1');
      });
    }
  });

};

exports.update = function (req, res) {
  var userId = req.params.userId;
  User.findOne({_id: mongoose.Types.ObjectId(userId)}, function (err, user) {
    if (err) return util.handleError(res, err);
    if (!user) return util.notFound(res, "no user");
    user = _.merge(user, req.body);
    user.save(function (err) {
      if (err) return util.handleError(res, err);
      util.handleOk(res);
    });
  });
};

exports.delete = function (req, res) {
  var userId = req.params.userId;
  User.findOne({_id: mongoose.Types.ObjectId(userId)}, function (err, user) {
    if (err) return util.handleError(res, err);
    if (!user) return util.notFound(res, "no user");
    user.remove(function (err) {
      if (err) return util.handleError(res, err);
      util.handleOk(res);
    });
  });
};

exports.signout = function (req, res){
  console.log(req.body);
  util.handleOk(res);
};

exports.authenticate = function (req, res) {

  User.findOne({
    username: req.body.username
  }, function (err, user) {

    if (err) {
      util.handleError(res, err);
    }

    if (!user) {

      return util.notAuthorized(res, 'Authentication failed.');
    } else {
      if (user) {
        // check if password matches
        user.comparePassword(req.body.password, function (isMatch) {
          if (!isMatch) {
            return util.notAuthorized(res, 'Authentication failed.');
          }
          var token = jwt.sign(user, config.secret, {
            expiresIn: 1440 * 60
          });

          // return the information including token as JSON
          return res.json({
            success: true,
            message: 'success',
            token: token
          });
        });

      }
    }

  });
};
