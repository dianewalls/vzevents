'use strict';

angular.module('tellme')
    .controller('ResetCtrl', function($scope, $auth, $rootScope, $location) {

        $scope.handlePwdResetBtnClick = function() {
            $auth.requestPasswordReset($scope.pwdResetForm)
                .then(function(resp) {
                    console.log(resp);
                })
                .catch(function(resp) {
                    console.log(resp);
                });
        };


    });
