'use strict';

var path = require('path'),
  express = require('express'),
  bodyParser = require('body-parser'),
  methodOverride = require('method-override'),
  cookieParser = require('cookie-parser'),
  config = require('./config.js'),
  root = config.root;

module.exports = function (app) {

  app.set('view engine', 'html');
  app.use(bodyParser.urlencoded({extended: false}));
  app.use(bodyParser.json());
  app.use(methodOverride());
  app.use(cookieParser());
  app.use(express.static(path.join(root, 'client')));
  app.set('appPath', path.join(root, 'client'));

};