'use strict';

angular.module('tellme')
  .service('AnimalsService', function ($http, $q) {
    this.list = function () {
      var deferred = $q.defer();
      $http.get('http://127.0.0.1:8080/ejemplo/rest/animals/listar').then(function (res) {
        console.log(res.data);
        var animales = res.data;
        deferred.resolve(animales);
      }, function () {
        deferred.reject({msg: 'Error while retrieving animals'});
      });
      return deferred.promise;
    };
  });