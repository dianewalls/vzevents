'use strict';

angular.module('tellme')
  .factory('Users', function ($http, $q) {
    return {
      list: function () {
        var deferred = $q.defer();
        $http.get('api/users').then(function (res) {
          var users = res.data;
          users.forEach(function (u) {
            u.age = 25;
          });
          deferred.resolve(users);
        }, function (res) {
          deferred.reject({msg: 'Error while retrieving users'});
        });
        return deferred.promise;
      }
    };
  });

angular.module('tellme')
  .service('UsersService', function ($http, $q) {
    this.list = function () {
      var deferred = $q.defer();
      $http.get('api/users').then(function (res) {
        var users = res.data;
        users.forEach(function (u) {
          u.age = 25;
        });
        deferred.resolve(users);
      }, function (res) {
        deferred.reject({msg: 'Error while retrieving users'});
      });
      return deferred.promise;
    };
  });